### Q: How can I contribute information to this site?
1. Join the project [here](https://git.cs.sun.ac.za/system-administrator/system-administrator.pages.cs.sun.ac.za/)

1. Please create an issue by going [here](https://git.cs.sun.ac.za/sulug/ubuntu-web/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

