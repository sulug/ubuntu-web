# Mounting SU network drives

SU staff and students have access to a number of network drives. The `H:\` drive on Windows represents a user's personal storage, and the `G:\` drive provides shared, departmental storage. The `P:\` drive contains software installation files (access to this drive may be limited). These drives can be mounted on Linux.

> The contents of the network drives you have access to can also be viewed and modified online at [storage.sun.ac.za](https://storage.sun.ac.za).

A set of packages is required to mount these drives on Linux. To install them, run the following:

```
sudo apt update
sudo apt install ldap-utils cifs-utils
```

Create a directory for the mount points inside your home directory:

```
cd
mkdir mnt
```

And create the corresponding directories for each mount point, e.g. in the case of your home directory:

```
mkdir mnt/$USER
```

Once this is done, you can use a script to mount each drive. The following example applies to the personal `H:\` drive:

> Note that the `$USER` and `$PASS` variables need to be set either in the script or exported in the shell prior to running the script. If your local username is the same as your network username you can set this automatically by adding `USER=$(whoami)` to the start of the script. To prompt for a password without displaying it in the shell, you can use `read -s -p "[LDAP] password for $USER: " PASS` after `$USER` has been set.

```
#!/bin/bash

while getopts ":u:m:h?" opt; do
	case "$opt" in
		u)
			USER=$OPTARG
			;;
		m)
			MOUNT=$OPTARG
			;;
		h|\?)
			echo "`basename `: mount home directory on NetApp"
			echo " Options:"
			echo "   -h         this help message"
			echo "   -u <USER>  username to use, defaults to $USER"
			echo "   -m <MOUNT> directory where to mount, defaults to $HOME/mnt/$USER"
			echo
			echo " Requires sudo access to use mount command"
			echo
			exit 0
	esac
done

MOUNT=${MOUNT:-$HOME/mnt/$USER}

if [ "/" != "${MOUNT::1}" ]; then
	MOUNT="`pwd`/$MOUNT"
fi

LDAP=`which ldapsearch`
CIFS=`which mount.cifs`

if [ -z "$LDAP" ]; then
	echo "ldapsearch not found, please install with one of the following commands"
	echo "Ubuntu (or Debian/Mint): aptitude install ldap-utils"
	echo "RedHat (or Fedora/Scientific): yum install openldap-clients"
	exit 1
fi

if [ -z "$CIFS" ]; then
	echo "mount.cifs not found, please install with one of the following commands"
	echo "Ubuntu (or Debian/Mint): aptitude install cifs-utils"
	echo "RedHat (or Fedora/Scientific): yum install cifs-utils"
	exit 1
fi

if [ ! -d $MOUNT ]; then
	mkdir -p $MOUNT
	if [ $? -eq 1 ]; then
		echo "error creating mountpoint $MOUNT"
		exit 1
	fi
elif [ -n "`mount | grep $MOUNT`" ]; then
	echo "already mounted"
	exit 1
fi

USER=ivdl
#read -s -p "[LDAP] password for $USER: " PASS
echo

OPTS="-x -H ldaps://stbad01.stb.sun.ac.za -b dc=stb,dc=sun,dc=ac,dc=za -D $USER@stb.sun.ac.za -w $PASS (sAMAccountName=$USER) homeDirectory"

OUTPUT=`LDAPTLS_REQCERT=allow $LDAP $OPTS`

if [ $? -eq 0 ]; then
	HOME=`echo "$OUTPUT" | grep ^homeDirectory | awk '{print $2}' | tr '\' '/' 2>/dev/null`

	if [ -z "$HOME" ]; then
		echo "unable to find home directory"
		exit 1
	fi

	echo "mounting $HOME on $MOUNT"

	OPTS="-t cifs -o sec=ntlm,username=$USER,password=$PASS,uid=$UID,vers=1.0"
	sudo mount $OPTS $HOME $MOUNT
else
	echo "incorrect password, unable to authenticate"
	exit 1
fi
```

> Note that the `vers=1.0` argument in the mount options is required, omitting it will result in an error.

To find the correct path to mount the shared `G:\` drive you can log on to Windows and view the details of the mounted drives. In general, the format of the path for the `G:\` drive is as follows:

```
//sunstaff.stb.sun.ac.za/dept/<faculty>/<department>
```

And the path for the `P:\` drive is as follows:

```
//146.232.77.125/pool
```
