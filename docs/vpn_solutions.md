# Campus VPN Solutions for Linux

## Rationale

Certain university services/servers are only accessible to students on the local university network, or on a wired ethernet connection.  A virtual private network (VPN) allows one to effectively connect to the university network remotely to be able to access these services from off campus or from Eduroam.

## Global Connect

Modern operating systems will use the SU PaloAlto GlobalProtect VPN via the network manager, or via GlobalProtect-openconnect.  For details on these setups, see [this documentation](https://system-administrator.pages.cs.sun.ac.za/globalprotect-openconnect/).

Information on legacy VPN solutions follows below.

## Citrix
### Support
* Works on Ubuntu 16.04 but that Ubuntu version will no longer be supported after April 2021. [See here](https://wiki.ubuntu.com/Releases)
* SecureVPN (The main VPN for campus wide access) does not work with 18.04 and later. You may have some success with the Environment specific VPN's. 

## Forticlient 
Officially deprecated, but kept on for situations where Citrix doesn't work, or until another solution is implemented. 

!!! warning 
    You need to request access to this VPN solution. Unless you know you've been given access, apply at IT's helpdek. 
    Let them know what Linux distribution and version you are using. 
    
    Link: https://servicedesk.sun.ac.za/jira/plugins/servlet/theme/portal/6/create/63

### Support:
* Ubuntu 18.04 and above (See [here](https://packages.ubuntu.com/search?suite=all&arch=any&searchon=packages&keywords=openfortivpn) for details.)
* For Debain, please see [the comment on this issue.](https://git.cs.sun.ac.za/sulug/ubuntu-web/-/issues/5#note_15948)
* Plenty of other OS's (Let us know if you have failures.) Search for `fortivpn` in your package repo. 

### Install and usage instructions:
#### Ubuntu
The below instructions may well work for Debian too. 

**Install:**

For the VPN Part:
``` bash
sudo apt install openfortivpn 
```

For the GUI add-in (I've found it to be less stable than running from command line)
Change `gnome` for your desktop env.
```
sudo apt install network-manager-fortisslvpn network-manager-fortisslvpn-gnome
```

**Details**

* Host: `fwvpn.sun.ac.za`
* Username: Your SUN username without @sun.ac.za
* Password: Your University Password
* Port: `443`
* Cert: None is required

**Run**

* Command line

    ```bash
    sudo openfortivpn fwvpn.sun.ac.za -u <username>
    ```


    !!! note 'DNS'
        In order to get DNS to work in the way you want, you may need to play around with adding the flags `--pppd-no-peerdns` and `--no-dns` to the command. 

* GUI
    * Open Network settings
    * Click the `+` next to the VPN section.
    * Choose "FortinetSSLVPN"
    * Fill in details from above, other details can be left blank.
